CERTMANAGER_CHART_VERSION="1.9.1"
CERTMANAGER_NAMESPACE="cert-manager"

# ------------------------------------------------

# Create namespace
kubectl create ns "${CERTMANAGER_NAMESPACE}" --dry-run=client -o yaml | kubectl apply -f -

# Install Cert Manager
helm repo add "jetstack" "https://charts.jetstack.io"
helm repo update
helm upgrade --install --atomic --debug "cert-manager" "jetstack/cert-manager" \
  --version "${CERTMANAGER_CHART_VERSION}" \
  --namespace "${CERTMANAGER_NAMESPACE}" \
  --timeout 600s \
  --set "installCRDs=true"

# Check status
until kubectl -n "${CERTMANAGER_NAMESPACE}" rollout status deployment cert-manager-webhook cert-manager-cainjector cert-manager
do
  echo "Not ready..."
  sleep 1
done
