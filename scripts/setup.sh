CERTMANAGER_NAMESPACE="cert-manager"
CLUSTERISSUER_NAME="clusterissuer"
VAULT_PKI_POLICY="k3s.lan"
VAULT_URL="https://vault.lan"
VAULT_CA_BUNDLE="-----BEGIN CERTIFICATE-----
MIIFLDCCAxSgAwIBAgIUCTY5sj/u4R46LUOnk+ayOFdL8ZcwDQYJKoZIhvcNAQEL
BQAwHDEaMBgGA1UEAxMRdmF1bHQubGFuIFJvb3QgQ0EwHhcNMjEwOTIyMTYzMzE1
WhcNNDEwOTE3MTYzMzQzWjAcMRowGAYDVQQDExF2YXVsdC5sYW4gUm9vdCBDQTCC
AiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAOVN/hpzDBXbRfskmddYlvp/
U6YkK8RFj0OhmJd8oVVq7mUaj2k0z6mywedyy26g2AGeb3pneqOakJizcZ49omQq
Z8IQIr0PDArwPVyUi/No31ABNb3GDG0Sfj3oXG9nqce5pgUWeFhUb+8U7OvBDoMl
+7VijkCpZWniiRYSduRMQjAKzH4bekOjXlhxd7V4TwNCTL4z9gE7/sMyJPwVE7Ar
DVoaKChe/oDbAZy8x5QXKzFzGKFCZZ5JF5x+Mh/uI97QW1W4I4P/wK6QeFAXZL+d
J9GnEHpnl54GdO2sryqdETdq7XCkigoA1TR8nNLW37TKZ3MBsqhe2jiB4RonFctJ
fZOVPgHavaZBgE0QkMJ5K0+jktBQJVAgWb9iIz1o2Zk/xOYr+OvL1k5o08IXq4wE
ddjOKPcBsPAeWW/pmIzCWeOPauBImtLB55oSJoawvfyqFC6OOfiQlMtXC9+Hjb3p
ODrz75wuXPNDTlo6woJHXFNZErZOMjzfuRLrnaJp8svzCaNQXauBJs8/giS2M/OE
NyixkOOhyeFuZjBbBl3iKDbdgyuYVmSPthVvXqbqE4fJQxposegT0DwsapDuBOyC
mWozmXw7Z4xeHqWzKb/sM+BGc6SLA5xmpSkuYd49RVtGIwdp9Q3LW8j3dLPPm+NR
NBGDlMbwq+S+agcziXPFAgMBAAGjZjBkMA4GA1UdDwEB/wQEAwIBBjASBgNVHRMB
Af8ECDAGAQH/AgEBMB0GA1UdDgQWBBTmN2YyzpgxQxzZWmvjV0FN9MJi3jAfBgNV
HSMEGDAWgBTmN2YyzpgxQxzZWmvjV0FN9MJi3jANBgkqhkiG9w0BAQsFAAOCAgEA
FmDV4dbLR3WzvRcq5z+XUtQug1PVyFQUW3HIWb/mFsxcw0EJ084owkb0fz0jCAmo
CWCJ9VlFeoJ8nTBlRBsmfA6uMe/jCjFwOBmF++k5HsLf7no8if8PXoOzcntRDI76
YXC1Xlh8kukvbu5NQYU0uc9ZDAUvTr5uev/I1W+UdUnOnWiQhargy/jXkA1pQ8ik
+3YYip05sJJlA4rndPLFEBV4qepblEFGI7YaZWB+ckN8GXyCHmKpE7w2Rjs/NBbz
9lHI0CJPZzvFb3hJKD6gfYA4UwQ8ULLsBR4dQ8cGY3FnfJMDvcdtP6FHasl/vN19
Enu1/3/ke9oWy5WoCT1JiO+Bo+6bFf4b0K+ghHyiJwthU4h39q8iWlj9gbZRHY11
qXJL5QlpEtp4dlJ1mrGeBqaou8uTC86VPQLIPqPWUT44bwB0MerlQcFTqLYvtcN7
cDTb15aOGIljkncTvBh0EXlOhyRvV07W19ii6yDmR3RWGpzLxgBMRFtmkiVVfr3M
TEc/aIdi4dHCLQ6UR6/jG6PBrjQOkr3SpDJml77f0DMFmP0yKRUJCkLgWtq0KS5Y
fTEkMTCmRjPDlX90qiylo0CiHUNOyqVABijOKILAd/CJmOMQ83iqjQbdO9wScvNl
Zw5b+lPtq4AH/ii98njU5D4yl253Vb59LXh/1+/vL+g=
-----END CERTIFICATE-----"

# ------------------------------------------------

: ${vault_approle_secretid:?not set}
: ${vault_approle_roleid:?not set}

# Create approle secret id
kubectl -n "${CERTMANAGER_NAMESPACE}" create secret generic "cert-manager-vault-approle" --from-literal=secretId="${vault_approle_secretid}" --dry-run=client -o yaml | kubectl apply -f -

# Create cluster issuer via vault with approle auth
cat << EOF | kubectl apply -f -
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: ${CLUSTERISSUER_NAME}
  namespace: ${CERTMANAGER_NAMESPACE}
spec:
  vault:
    path: pki/sign/${VAULT_PKI_POLICY}
    server: "${VAULT_URL}"
    caBundle: $(echo "${VAULT_CA_BUNDLE}" | base64 -w 0)
    auth:
      appRole:
        path: approle
        roleId: ${vault_approle_roleid}
        secretRef:
          name: cert-manager-vault-approle
          key: secretId
EOF